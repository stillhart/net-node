# frozen_string_literal: true

require_relative "lib/net/node/version"

Gem::Specification.new do |spec|
  spec.name          = "net-node"
  spec.version       = Net::Node::VERSION
  spec.authors       = ["Fabian"]
  spec.email         = ["fabian_stillhart@hotmail.com"]

  spec.summary       = "A gem to provide a standard interface to interact with various Net Gems"
  # spec.homepage      = "TODO: Put your gem's website or public repo URL here."
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  # spec.metadata["homepage_uri"] = spec.homepage
  # spec.metadata["source_code_uri"] = "TODO: Put your gem's public repo URL here."
  # spec.metadata["changelog_uri"] = "TODO: Put your gem's CHANGELOG.md URL here."

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
  spec.add_dependency "activesupport"
  spec.add_dependency "net-config"
  spec.add_dependency "net-scp"
  spec.add_dependency "net-sftp"
  spec.add_dependency "net-ssh"
  spec.add_dependency "net-ssh-cli"
  spec.add_dependency "netsnmp"
end
