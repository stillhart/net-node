FROM ubuntu:latest
RUN apt update -y && apt install openssh-server -y
RUN sed -i 's/#PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config
RUN useradd config -m -p config
RUN openssh-server
