# frozen_string_literal: true

require "net/node/version"
require "net/config"
require "net/ssh"
require "net/sftp"
require "net/scp"
require "netsnmp"
require "active_support/all"
require "securerandom"

module Net
  module NodeModule
    class Error < StandardError; end

    attr_accessor :ssh, :cli, :clis, :sftp, :scp, :snmp
    attr_accessor :config

    def initialize(config: {})
      self.config = Net::Config.new(config: config)
      self.clis = ActiveSupport::HashWithIndifferentAccess.new
    end

    def ssh
      @ssh ||= start_ssh
    end

    def cli(id: :default)
      clis[id] ||= ssh.cli
    end

    def sftp
      @sftp ||= ssh.sftp
    end

    def scp
      @scp ||= ssh.scp
    end

    def close
      @clis.each { |_, channel| channel&.close }
      @sftp&.close
      @scp&.close
    end

    # def snmp
    # end

    # def http
    # end

    def net_ssh_options
      [
        config.fetch(:net, :ssh, :ip) || config.fetch(:net, :ssh, :hostname),
        config.fetch(:net, :ssh, :user, default: ENV["USER"]),
        config.fetch(:net, :ssh, :options, default: {}).symbolize_keys
      ]
    end

    def start_ssh
      Net::SSH.start(*net_ssh_options)
    end

    def net_ssh_cli_options
      { net_ssh: net_ssh }.merge(config.fetch(:net, :ssh, :cli, :options))
    end

    def add_cli(id: SecureRandom.hex(5))
      clis[id] = Net::SSH::CLI::Session.new(**net_ssh_cli_options)
    end
  end

  module NodesModule
  end

  module ConfigModule
  end
end

module Net
  class Node
    include NodeModule
  end

  class Nodes
    include NodesModule
  end

  class Config
    include ConfigModule
  end
end
