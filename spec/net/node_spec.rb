# frozen_string_literal: true

require "net/node"
RSpec.describe Net::Node do
  it "has a version number" do
    expect(Net::Node::VERSION).not_to be nil
  end

  let(:node) do
    node = Net::Node.new
    node.config.load_paths = [File.join(__dir__, "net_config")]
    node.config.facts = { hostname: "localhost" }
    node.config.merge_files_by_facts
    node
  end

  it "is an instance" do
    expect(node).to be_a(Net::Node)
  end

  context "ssh" do
    after(:each) { node.close }

    it "has net_ssh_options" do
      expect(node.net_ssh_options).to be_a(Array)
      expect(node.config.fetch(:net, :ssh, :options)).to be_a(Hash)
    end

    it "connects to ssh" do
      expect(node.ssh).to be_a(Net::SSH::Connection)
    end
  end
end
